var callers = {
  "contractName": "ApprovedCallers",
  "abi": [
    {
      "inputs": [],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "constructor",
      "signature": "constructor"
    },
    {
      "anonymous": false,
      "inputs": [
        {
          "indexed": true,
          "name": "_approved",
          "type": "address"
        }
      ],
      "name": "AddedApproved",
      "type": "event",
      "signature": "0x4d8e6ba25400c3feba1ad107e99ee8bf523b9b3ed7f08d4fe26f44993363bc5d"
    },
    {
      "constant": false,
      "inputs": [
        {
          "name": "txt",
          "type": "string"
        }
      ],
      "name": "setSomeText",
      "outputs": [],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "function",
      "signature": "0x76841948"
    },
    {
      "constant": false,
      "inputs": [
        {
          "name": "_approved",
          "type": "address"
        }
      ],
      "name": "addApprovedAddress",
      "outputs": [],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "function",
      "signature": "0xd2ac1c8e"
    },
    {
      "constant": true,
      "inputs": [
        {
          "name": "_addr",
          "type": "address"
        }
      ],
      "name": "isAddressApproved",
      "outputs": [
        {
          "name": "",
          "type": "bool"
        }
      ],
      "payable": false,
      "stateMutability": "view",
      "type": "function",
      "signature": "0xcc83430f"
    }
  ],
  "bytecode": "0x608060405234801561001057600080fd5b5060016000803373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060006101000a81548160ff021916908315150217905550610413806100776000396000f3fe608060405260043610610051576000357c0100000000000000000000000000000000000000000000000000000000900480637684194814610056578063cc83430f1461011e578063d2ac1c8e14610187575b600080fd5b34801561006257600080fd5b5061011c6004803603602081101561007957600080fd5b810190808035906020019064010000000081111561009657600080fd5b8201836020820111156100a857600080fd5b803590602001918460018302840111640100000000831117156100ca57600080fd5b91908080601f016020809104026020016040519081016040528093929190818152602001838380828437600081840152601f19601f8201169050808301925050505050505091929192905050506101d8565b005b34801561012a57600080fd5b5061016d6004803603602081101561014157600080fd5b81019080803573ffffffffffffffffffffffffffffffffffffffff1690602001909291905050506101f2565b604051808215151515815260200191505060405180910390f35b34801561019357600080fd5b506101d6600480360360208110156101aa57600080fd5b81019080803573ffffffffffffffffffffffffffffffffffffffff169060200190929190505050610247565b005b80600190805190602001906101ee929190610342565b5050565b60008060008373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060009054906101000a900460ff169050919050565b600115156000803373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060009054906101000a900460ff1615151415156102a557600080fd5b60016000808373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060006101000a81548160ff0219169083151502179055508073ffffffffffffffffffffffffffffffffffffffff167f4d8e6ba25400c3feba1ad107e99ee8bf523b9b3ed7f08d4fe26f44993363bc5d60405160405180910390a250565b828054600181600116156101000203166002900490600052602060002090601f016020900481019282601f1061038357805160ff19168380011785556103b1565b828001600101855582156103b1579182015b828111156103b0578251825591602001919060010190610395565b5b5090506103be91906103c2565b5090565b6103e491905b808211156103e05760008160009055506001016103c8565b5090565b9056fea165627a7a72305820a61009cac20d8b6db47a017603b51565e48c83ef2d3a2096800dc2b68d6a1e760029",
  "deployedBytecode": "0x608060405260043610610051576000357c0100000000000000000000000000000000000000000000000000000000900480637684194814610056578063cc83430f1461011e578063d2ac1c8e14610187575b600080fd5b34801561006257600080fd5b5061011c6004803603602081101561007957600080fd5b810190808035906020019064010000000081111561009657600080fd5b8201836020820111156100a857600080fd5b803590602001918460018302840111640100000000831117156100ca57600080fd5b91908080601f016020809104026020016040519081016040528093929190818152602001838380828437600081840152601f19601f8201169050808301925050505050505091929192905050506101d8565b005b34801561012a57600080fd5b5061016d6004803603602081101561014157600080fd5b81019080803573ffffffffffffffffffffffffffffffffffffffff1690602001909291905050506101f2565b604051808215151515815260200191505060405180910390f35b34801561019357600080fd5b506101d6600480360360208110156101aa57600080fd5b81019080803573ffffffffffffffffffffffffffffffffffffffff169060200190929190505050610247565b005b80600190805190602001906101ee929190610342565b5050565b60008060008373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060009054906101000a900460ff169050919050565b600115156000803373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060009054906101000a900460ff1615151415156102a557600080fd5b60016000808373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060006101000a81548160ff0219169083151502179055508073ffffffffffffffffffffffffffffffffffffffff167f4d8e6ba25400c3feba1ad107e99ee8bf523b9b3ed7f08d4fe26f44993363bc5d60405160405180910390a250565b828054600181600116156101000203166002900490600052602060002090601f016020900481019282601f1061038357805160ff19168380011785556103b1565b828001600101855582156103b1579182015b828111156103b0578251825591602001919060010190610395565b5b5090506103be91906103c2565b5090565b6103e491905b808211156103e05760008160009055506001016103c8565b5090565b9056fea165627a7a72305820a61009cac20d8b6db47a017603b51565e48c83ef2d3a2096800dc2b68d6a1e760029",
  "sourceMap": "77:594:0:-;;;265:61;8:9:-1;5:2;;;30:1;27;20:12;5:2;265:61:0;316:4;293:8;:20;302:10;293:20;;;;;;;;;;;;;;;;:27;;;;;;;;;;;;;;;;;;77:594;;;;;;",
  "deployedSourceMap": "77:594:0:-;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;330:71;;8:9:-1;5:2;;;30:1;27;20:12;5:2;330:71:0;;;;;;13:2:-1;8:3;5:11;2:2;;;29:1;26;19:12;2:2;330:71:0;;;;;;;;;;21:11:-1;8;5:28;2:2;;;46:1;43;36:12;2:2;330:71:0;;35:9:-1;28:4;12:14;8:25;5:40;2:2;;;58:1;55;48:12;2:2;330:71:0;;;;;;100:9:-1;95:1;81:12;77:20;67:8;63:35;60:50;39:11;25:12;22:29;11:107;8:2;;;131:1;128;121:12;8:2;330:71:0;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;30:3:-1;22:6;14;1:33;99:1;93:3;85:6;81:16;74:27;137:4;133:9;126:4;121:3;117:14;113:30;106:37;;169:3;161:6;157:16;147:26;;330:71:0;;;;;;;;;;;;;;;:::i;:::-;;564:104;;8:9:-1;5:2;;;30:1;27;20:12;5:2;564:104:0;;;;;;13:2:-1;8:3;5:11;2:2;;;29:1;26;19:12;2:2;564:104:0;;;;;;;;;;;;;;;;;;;:::i;:::-;;;;;;;;;;;;;;;;;;;;;;;405:142;;8:9:-1;5:2;;;30:1;27;20:12;5:2;405:142:0;;;;;;13:2:-1;8:3;5:11;2:2;;;29:1;26;19:12;2:2;405:142:0;;;;;;;;;;;;;;;;;;;:::i;:::-;;330:71;392:3;384:5;:11;;;;;;;;;;;;:::i;:::-;;330:71;:::o;564:104::-;627:4;647:8;:15;656:5;647:15;;;;;;;;;;;;;;;;;;;;;;;;;640:22;;564:104;;;:::o;405:142::-;226:4;202:28;;:8;:20;211:10;202:20;;;;;;;;;;;;;;;;;;;;;;;;;:28;;;194:37;;;;;;;;501:4;479:8;:19;488:9;479:19;;;;;;;;;;;;;;;;:26;;;;;;;;;;;;;;;;;;531:9;517:24;;;;;;;;;;;;405:142;:::o;77:594::-;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;:::i;:::-;;;:::o;:::-;;;;;;;;;;;;;;;;;;;;;;;;;;;:::o",
  "source": "pragma solidity 0.5.1;\n\nimport \"contracts/interfaces/IApprovedCallers.sol\";\n\ncontract ApprovedCallers is IApprovedCallers {\n\n  mapping(address => bool) approved;\n\n  modifier onlyApproved {\n\n    require(approved[msg.sender] == true);\n    _;\n\n  }\n\n  string toSet;\n\n  constructor() public {\n\n    approved[msg.sender] = true;\n\n  }\n\n  function setSomeText(string memory txt) public {\n\n    toSet = txt;\n\n  }\n\n  function addApprovedAddress(address _approved) public onlyApproved {\n\n    approved[_approved] = true;\n\n    emit AddedApproved(_approved);\n\n  }\n\n  //GETTERS\n\n  function isAddressApproved(address _addr) public view returns (bool) {\n\n    return approved[_addr];\n\n  }\n\n}\n",
  "sourcePath": "/home/stefan/Work/Quorum-Messenger-App/chain/contracts/ApprovedCallers.sol",
  "ast": {
    "absolutePath": "/home/stefan/Work/Quorum-Messenger-App/chain/contracts/ApprovedCallers.sol",
    "exportedSymbols": {
      "ApprovedCallers": [
        75
      ]
    },
    "id": 76,
    "nodeType": "SourceUnit",
    "nodes": [
      {
        "id": 1,
        "literals": [
          "solidity",
          "0.5",
          ".1"
        ],
        "nodeType": "PragmaDirective",
        "src": "0:22:0"
      },
      {
        "absolutePath": "contracts/interfaces/IApprovedCallers.sol",
        "file": "contracts/interfaces/IApprovedCallers.sol",
        "id": 2,
        "nodeType": "ImportDirective",
        "scope": 76,
        "sourceUnit": 1046,
        "src": "24:51:0",
        "symbolAliases": [],
        "unitAlias": ""
      },
      {
        "baseContracts": [
          {
            "arguments": null,
            "baseName": {
              "contractScope": null,
              "id": 3,
              "name": "IApprovedCallers",
              "nodeType": "UserDefinedTypeName",
              "referencedDeclaration": 1045,
              "src": "105:16:0",
              "typeDescriptions": {
                "typeIdentifier": "t_contract$_IApprovedCallers_$1045",
                "typeString": "contract IApprovedCallers"
              }
            },
            "id": 4,
            "nodeType": "InheritanceSpecifier",
            "src": "105:16:0"
          }
        ],
        "contractDependencies": [
          1045
        ],
        "contractKind": "contract",
        "documentation": null,
        "fullyImplemented": true,
        "id": 75,
        "linearizedBaseContracts": [
          75,
          1045
        ],
        "name": "ApprovedCallers",
        "nodeType": "ContractDefinition",
        "nodes": [
          {
            "constant": false,
            "id": 8,
            "name": "approved",
            "nodeType": "VariableDeclaration",
            "scope": 75,
            "src": "127:33:0",
            "stateVariable": true,
            "storageLocation": "default",
            "typeDescriptions": {
              "typeIdentifier": "t_mapping$_t_address_$_t_bool_$",
              "typeString": "mapping(address => bool)"
            },
            "typeName": {
              "id": 7,
              "keyType": {
                "id": 5,
                "name": "address",
                "nodeType": "ElementaryTypeName",
                "src": "135:7:0",
                "typeDescriptions": {
                  "typeIdentifier": "t_address",
                  "typeString": "address"
                }
              },
              "nodeType": "Mapping",
              "src": "127:24:0",
              "typeDescriptions": {
                "typeIdentifier": "t_mapping$_t_address_$_t_bool_$",
                "typeString": "mapping(address => bool)"
              },
              "valueType": {
                "id": 6,
                "name": "bool",
                "nodeType": "ElementaryTypeName",
                "src": "146:4:0",
                "typeDescriptions": {
                  "typeIdentifier": "t_bool",
                  "typeString": "bool"
                }
              }
            },
            "value": null,
            "visibility": "internal"
          },
          {
            "body": {
              "id": 20,
              "nodeType": "Block",
              "src": "187:57:0",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "commonType": {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        },
                        "id": 16,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": false,
                        "lValueRequested": false,
                        "leftExpression": {
                          "argumentTypes": null,
                          "baseExpression": {
                            "argumentTypes": null,
                            "id": 11,
                            "name": "approved",
                            "nodeType": "Identifier",
                            "overloadedDeclarations": [],
                            "referencedDeclaration": 8,
                            "src": "202:8:0",
                            "typeDescriptions": {
                              "typeIdentifier": "t_mapping$_t_address_$_t_bool_$",
                              "typeString": "mapping(address => bool)"
                            }
                          },
                          "id": 14,
                          "indexExpression": {
                            "argumentTypes": null,
                            "expression": {
                              "argumentTypes": null,
                              "id": 12,
                              "name": "msg",
                              "nodeType": "Identifier",
                              "overloadedDeclarations": [],
                              "referencedDeclaration": 1215,
                              "src": "211:3:0",
                              "typeDescriptions": {
                                "typeIdentifier": "t_magic_message",
                                "typeString": "msg"
                              }
                            },
                            "id": 13,
                            "isConstant": false,
                            "isLValue": false,
                            "isPure": false,
                            "lValueRequested": false,
                            "memberName": "sender",
                            "nodeType": "MemberAccess",
                            "referencedDeclaration": null,
                            "src": "211:10:0",
                            "typeDescriptions": {
                              "typeIdentifier": "t_address_payable",
                              "typeString": "address payable"
                            }
                          },
                          "isConstant": false,
                          "isLValue": true,
                          "isPure": false,
                          "lValueRequested": false,
                          "nodeType": "IndexAccess",
                          "src": "202:20:0",
                          "typeDescriptions": {
                            "typeIdentifier": "t_bool",
                            "typeString": "bool"
                          }
                        },
                        "nodeType": "BinaryOperation",
                        "operator": "==",
                        "rightExpression": {
                          "argumentTypes": null,
                          "hexValue": "74727565",
                          "id": 15,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": true,
                          "kind": "bool",
                          "lValueRequested": false,
                          "nodeType": "Literal",
                          "src": "226:4:0",
                          "subdenomination": null,
                          "typeDescriptions": {
                            "typeIdentifier": "t_bool",
                            "typeString": "bool"
                          },
                          "value": "true"
                        },
                        "src": "202:28:0",
                        "typeDescriptions": {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        }
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        }
                      ],
                      "id": 10,
                      "name": "require",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [
                        1218,
                        1219
                      ],
                      "referencedDeclaration": 1218,
                      "src": "194:7:0",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_require_pure$_t_bool_$returns$__$",
                        "typeString": "function (bool) pure"
                      }
                    },
                    "id": 17,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "194:37:0",
                    "typeDescriptions": {
                      "typeIdentifier": "t_tuple$__$",
                      "typeString": "tuple()"
                    }
                  },
                  "id": 18,
                  "nodeType": "ExpressionStatement",
                  "src": "194:37:0"
                },
                {
                  "id": 19,
                  "nodeType": "PlaceholderStatement",
                  "src": "237:1:0"
                }
              ]
            },
            "documentation": null,
            "id": 21,
            "name": "onlyApproved",
            "nodeType": "ModifierDefinition",
            "parameters": {
              "id": 9,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "187:0:0"
            },
            "src": "165:79:0",
            "visibility": "internal"
          },
          {
            "constant": false,
            "id": 23,
            "name": "toSet",
            "nodeType": "VariableDeclaration",
            "scope": 75,
            "src": "248:12:0",
            "stateVariable": true,
            "storageLocation": "default",
            "typeDescriptions": {
              "typeIdentifier": "t_string_storage",
              "typeString": "string"
            },
            "typeName": {
              "id": 22,
              "name": "string",
              "nodeType": "ElementaryTypeName",
              "src": "248:6:0",
              "typeDescriptions": {
                "typeIdentifier": "t_string_storage_ptr",
                "typeString": "string"
              }
            },
            "value": null,
            "visibility": "internal"
          },
          {
            "body": {
              "id": 33,
              "nodeType": "Block",
              "src": "286:40:0",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "id": 31,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "leftHandSide": {
                      "argumentTypes": null,
                      "baseExpression": {
                        "argumentTypes": null,
                        "id": 26,
                        "name": "approved",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 8,
                        "src": "293:8:0",
                        "typeDescriptions": {
                          "typeIdentifier": "t_mapping$_t_address_$_t_bool_$",
                          "typeString": "mapping(address => bool)"
                        }
                      },
                      "id": 29,
                      "indexExpression": {
                        "argumentTypes": null,
                        "expression": {
                          "argumentTypes": null,
                          "id": 27,
                          "name": "msg",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 1215,
                          "src": "302:3:0",
                          "typeDescriptions": {
                            "typeIdentifier": "t_magic_message",
                            "typeString": "msg"
                          }
                        },
                        "id": 28,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": false,
                        "lValueRequested": false,
                        "memberName": "sender",
                        "nodeType": "MemberAccess",
                        "referencedDeclaration": null,
                        "src": "302:10:0",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address_payable",
                          "typeString": "address payable"
                        }
                      },
                      "isConstant": false,
                      "isLValue": true,
                      "isPure": false,
                      "lValueRequested": true,
                      "nodeType": "IndexAccess",
                      "src": "293:20:0",
                      "typeDescriptions": {
                        "typeIdentifier": "t_bool",
                        "typeString": "bool"
                      }
                    },
                    "nodeType": "Assignment",
                    "operator": "=",
                    "rightHandSide": {
                      "argumentTypes": null,
                      "hexValue": "74727565",
                      "id": 30,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": true,
                      "kind": "bool",
                      "lValueRequested": false,
                      "nodeType": "Literal",
                      "src": "316:4:0",
                      "subdenomination": null,
                      "typeDescriptions": {
                        "typeIdentifier": "t_bool",
                        "typeString": "bool"
                      },
                      "value": "true"
                    },
                    "src": "293:27:0",
                    "typeDescriptions": {
                      "typeIdentifier": "t_bool",
                      "typeString": "bool"
                    }
                  },
                  "id": 32,
                  "nodeType": "ExpressionStatement",
                  "src": "293:27:0"
                }
              ]
            },
            "documentation": null,
            "id": 34,
            "implemented": true,
            "kind": "constructor",
            "modifiers": [],
            "name": "",
            "nodeType": "FunctionDefinition",
            "parameters": {
              "id": 24,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "276:2:0"
            },
            "returnParameters": {
              "id": 25,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "286:0:0"
            },
            "scope": 75,
            "src": "265:61:0",
            "stateMutability": "nonpayable",
            "superFunction": null,
            "visibility": "public"
          },
          {
            "body": {
              "id": 43,
              "nodeType": "Block",
              "src": "377:24:0",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "id": 41,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "leftHandSide": {
                      "argumentTypes": null,
                      "id": 39,
                      "name": "toSet",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 23,
                      "src": "384:5:0",
                      "typeDescriptions": {
                        "typeIdentifier": "t_string_storage",
                        "typeString": "string storage ref"
                      }
                    },
                    "nodeType": "Assignment",
                    "operator": "=",
                    "rightHandSide": {
                      "argumentTypes": null,
                      "id": 40,
                      "name": "txt",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 36,
                      "src": "392:3:0",
                      "typeDescriptions": {
                        "typeIdentifier": "t_string_memory_ptr",
                        "typeString": "string memory"
                      }
                    },
                    "src": "384:11:0",
                    "typeDescriptions": {
                      "typeIdentifier": "t_string_storage",
                      "typeString": "string storage ref"
                    }
                  },
                  "id": 42,
                  "nodeType": "ExpressionStatement",
                  "src": "384:11:0"
                }
              ]
            },
            "documentation": null,
            "id": 44,
            "implemented": true,
            "kind": "function",
            "modifiers": [],
            "name": "setSomeText",
            "nodeType": "FunctionDefinition",
            "parameters": {
              "id": 37,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 36,
                  "name": "txt",
                  "nodeType": "VariableDeclaration",
                  "scope": 44,
                  "src": "351:17:0",
                  "stateVariable": false,
                  "storageLocation": "memory",
                  "typeDescriptions": {
                    "typeIdentifier": "t_string_memory_ptr",
                    "typeString": "string"
                  },
                  "typeName": {
                    "id": 35,
                    "name": "string",
                    "nodeType": "ElementaryTypeName",
                    "src": "351:6:0",
                    "typeDescriptions": {
                      "typeIdentifier": "t_string_storage_ptr",
                      "typeString": "string"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "350:19:0"
            },
            "returnParameters": {
              "id": 38,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "377:0:0"
            },
            "scope": 75,
            "src": "330:71:0",
            "stateMutability": "nonpayable",
            "superFunction": null,
            "visibility": "public"
          },
          {
            "body": {
              "id": 61,
              "nodeType": "Block",
              "src": "472:75:0",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "id": 55,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "leftHandSide": {
                      "argumentTypes": null,
                      "baseExpression": {
                        "argumentTypes": null,
                        "id": 51,
                        "name": "approved",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 8,
                        "src": "479:8:0",
                        "typeDescriptions": {
                          "typeIdentifier": "t_mapping$_t_address_$_t_bool_$",
                          "typeString": "mapping(address => bool)"
                        }
                      },
                      "id": 53,
                      "indexExpression": {
                        "argumentTypes": null,
                        "id": 52,
                        "name": "_approved",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 46,
                        "src": "488:9:0",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      },
                      "isConstant": false,
                      "isLValue": true,
                      "isPure": false,
                      "lValueRequested": true,
                      "nodeType": "IndexAccess",
                      "src": "479:19:0",
                      "typeDescriptions": {
                        "typeIdentifier": "t_bool",
                        "typeString": "bool"
                      }
                    },
                    "nodeType": "Assignment",
                    "operator": "=",
                    "rightHandSide": {
                      "argumentTypes": null,
                      "hexValue": "74727565",
                      "id": 54,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": true,
                      "kind": "bool",
                      "lValueRequested": false,
                      "nodeType": "Literal",
                      "src": "501:4:0",
                      "subdenomination": null,
                      "typeDescriptions": {
                        "typeIdentifier": "t_bool",
                        "typeString": "bool"
                      },
                      "value": "true"
                    },
                    "src": "479:26:0",
                    "typeDescriptions": {
                      "typeIdentifier": "t_bool",
                      "typeString": "bool"
                    }
                  },
                  "id": 56,
                  "nodeType": "ExpressionStatement",
                  "src": "479:26:0"
                },
                {
                  "eventCall": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "id": 58,
                        "name": "_approved",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 46,
                        "src": "531:9:0",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      ],
                      "id": 57,
                      "name": "AddedApproved",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 1032,
                      "src": "517:13:0",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_event_nonpayable$_t_address_$returns$__$",
                        "typeString": "function (address)"
                      }
                    },
                    "id": 59,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "517:24:0",
                    "typeDescriptions": {
                      "typeIdentifier": "t_tuple$__$",
                      "typeString": "tuple()"
                    }
                  },
                  "id": 60,
                  "nodeType": "EmitStatement",
                  "src": "512:29:0"
                }
              ]
            },
            "documentation": null,
            "id": 62,
            "implemented": true,
            "kind": "function",
            "modifiers": [
              {
                "arguments": null,
                "id": 49,
                "modifierName": {
                  "argumentTypes": null,
                  "id": 48,
                  "name": "onlyApproved",
                  "nodeType": "Identifier",
                  "overloadedDeclarations": [],
                  "referencedDeclaration": 21,
                  "src": "459:12:0",
                  "typeDescriptions": {
                    "typeIdentifier": "t_modifier$__$",
                    "typeString": "modifier ()"
                  }
                },
                "nodeType": "ModifierInvocation",
                "src": "459:12:0"
              }
            ],
            "name": "addApprovedAddress",
            "nodeType": "FunctionDefinition",
            "parameters": {
              "id": 47,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 46,
                  "name": "_approved",
                  "nodeType": "VariableDeclaration",
                  "scope": 62,
                  "src": "433:17:0",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_address",
                    "typeString": "address"
                  },
                  "typeName": {
                    "id": 45,
                    "name": "address",
                    "nodeType": "ElementaryTypeName",
                    "src": "433:7:0",
                    "stateMutability": "nonpayable",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "432:19:0"
            },
            "returnParameters": {
              "id": 50,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "472:0:0"
            },
            "scope": 75,
            "src": "405:142:0",
            "stateMutability": "nonpayable",
            "superFunction": 1037,
            "visibility": "public"
          },
          {
            "body": {
              "id": 73,
              "nodeType": "Block",
              "src": "633:35:0",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "baseExpression": {
                      "argumentTypes": null,
                      "id": 69,
                      "name": "approved",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 8,
                      "src": "647:8:0",
                      "typeDescriptions": {
                        "typeIdentifier": "t_mapping$_t_address_$_t_bool_$",
                        "typeString": "mapping(address => bool)"
                      }
                    },
                    "id": 71,
                    "indexExpression": {
                      "argumentTypes": null,
                      "id": 70,
                      "name": "_addr",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 64,
                      "src": "656:5:0",
                      "typeDescriptions": {
                        "typeIdentifier": "t_address",
                        "typeString": "address"
                      }
                    },
                    "isConstant": false,
                    "isLValue": true,
                    "isPure": false,
                    "lValueRequested": false,
                    "nodeType": "IndexAccess",
                    "src": "647:15:0",
                    "typeDescriptions": {
                      "typeIdentifier": "t_bool",
                      "typeString": "bool"
                    }
                  },
                  "functionReturnParameters": 68,
                  "id": 72,
                  "nodeType": "Return",
                  "src": "640:22:0"
                }
              ]
            },
            "documentation": null,
            "id": 74,
            "implemented": true,
            "kind": "function",
            "modifiers": [],
            "name": "isAddressApproved",
            "nodeType": "FunctionDefinition",
            "parameters": {
              "id": 65,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 64,
                  "name": "_addr",
                  "nodeType": "VariableDeclaration",
                  "scope": 74,
                  "src": "591:13:0",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_address",
                    "typeString": "address"
                  },
                  "typeName": {
                    "id": 63,
                    "name": "address",
                    "nodeType": "ElementaryTypeName",
                    "src": "591:7:0",
                    "stateMutability": "nonpayable",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "590:15:0"
            },
            "returnParameters": {
              "id": 68,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 67,
                  "name": "",
                  "nodeType": "VariableDeclaration",
                  "scope": 74,
                  "src": "627:4:0",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_bool",
                    "typeString": "bool"
                  },
                  "typeName": {
                    "id": 66,
                    "name": "bool",
                    "nodeType": "ElementaryTypeName",
                    "src": "627:4:0",
                    "typeDescriptions": {
                      "typeIdentifier": "t_bool",
                      "typeString": "bool"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "626:6:0"
            },
            "scope": 75,
            "src": "564:104:0",
            "stateMutability": "view",
            "superFunction": 1044,
            "visibility": "public"
          }
        ],
        "scope": 76,
        "src": "77:594:0"
      }
    ],
    "src": "0:672:0"
  },
  "legacyAST": {
    "absolutePath": "/home/stefan/Work/Quorum-Messenger-App/chain/contracts/ApprovedCallers.sol",
    "exportedSymbols": {
      "ApprovedCallers": [
        75
      ]
    },
    "id": 76,
    "nodeType": "SourceUnit",
    "nodes": [
      {
        "id": 1,
        "literals": [
          "solidity",
          "0.5",
          ".1"
        ],
        "nodeType": "PragmaDirective",
        "src": "0:22:0"
      },
      {
        "absolutePath": "contracts/interfaces/IApprovedCallers.sol",
        "file": "contracts/interfaces/IApprovedCallers.sol",
        "id": 2,
        "nodeType": "ImportDirective",
        "scope": 76,
        "sourceUnit": 1046,
        "src": "24:51:0",
        "symbolAliases": [],
        "unitAlias": ""
      },
      {
        "baseContracts": [
          {
            "arguments": null,
            "baseName": {
              "contractScope": null,
              "id": 3,
              "name": "IApprovedCallers",
              "nodeType": "UserDefinedTypeName",
              "referencedDeclaration": 1045,
              "src": "105:16:0",
              "typeDescriptions": {
                "typeIdentifier": "t_contract$_IApprovedCallers_$1045",
                "typeString": "contract IApprovedCallers"
              }
            },
            "id": 4,
            "nodeType": "InheritanceSpecifier",
            "src": "105:16:0"
          }
        ],
        "contractDependencies": [
          1045
        ],
        "contractKind": "contract",
        "documentation": null,
        "fullyImplemented": true,
        "id": 75,
        "linearizedBaseContracts": [
          75,
          1045
        ],
        "name": "ApprovedCallers",
        "nodeType": "ContractDefinition",
        "nodes": [
          {
            "constant": false,
            "id": 8,
            "name": "approved",
            "nodeType": "VariableDeclaration",
            "scope": 75,
            "src": "127:33:0",
            "stateVariable": true,
            "storageLocation": "default",
            "typeDescriptions": {
              "typeIdentifier": "t_mapping$_t_address_$_t_bool_$",
              "typeString": "mapping(address => bool)"
            },
            "typeName": {
              "id": 7,
              "keyType": {
                "id": 5,
                "name": "address",
                "nodeType": "ElementaryTypeName",
                "src": "135:7:0",
                "typeDescriptions": {
                  "typeIdentifier": "t_address",
                  "typeString": "address"
                }
              },
              "nodeType": "Mapping",
              "src": "127:24:0",
              "typeDescriptions": {
                "typeIdentifier": "t_mapping$_t_address_$_t_bool_$",
                "typeString": "mapping(address => bool)"
              },
              "valueType": {
                "id": 6,
                "name": "bool",
                "nodeType": "ElementaryTypeName",
                "src": "146:4:0",
                "typeDescriptions": {
                  "typeIdentifier": "t_bool",
                  "typeString": "bool"
                }
              }
            },
            "value": null,
            "visibility": "internal"
          },
          {
            "body": {
              "id": 20,
              "nodeType": "Block",
              "src": "187:57:0",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "commonType": {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        },
                        "id": 16,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": false,
                        "lValueRequested": false,
                        "leftExpression": {
                          "argumentTypes": null,
                          "baseExpression": {
                            "argumentTypes": null,
                            "id": 11,
                            "name": "approved",
                            "nodeType": "Identifier",
                            "overloadedDeclarations": [],
                            "referencedDeclaration": 8,
                            "src": "202:8:0",
                            "typeDescriptions": {
                              "typeIdentifier": "t_mapping$_t_address_$_t_bool_$",
                              "typeString": "mapping(address => bool)"
                            }
                          },
                          "id": 14,
                          "indexExpression": {
                            "argumentTypes": null,
                            "expression": {
                              "argumentTypes": null,
                              "id": 12,
                              "name": "msg",
                              "nodeType": "Identifier",
                              "overloadedDeclarations": [],
                              "referencedDeclaration": 1215,
                              "src": "211:3:0",
                              "typeDescriptions": {
                                "typeIdentifier": "t_magic_message",
                                "typeString": "msg"
                              }
                            },
                            "id": 13,
                            "isConstant": false,
                            "isLValue": false,
                            "isPure": false,
                            "lValueRequested": false,
                            "memberName": "sender",
                            "nodeType": "MemberAccess",
                            "referencedDeclaration": null,
                            "src": "211:10:0",
                            "typeDescriptions": {
                              "typeIdentifier": "t_address_payable",
                              "typeString": "address payable"
                            }
                          },
                          "isConstant": false,
                          "isLValue": true,
                          "isPure": false,
                          "lValueRequested": false,
                          "nodeType": "IndexAccess",
                          "src": "202:20:0",
                          "typeDescriptions": {
                            "typeIdentifier": "t_bool",
                            "typeString": "bool"
                          }
                        },
                        "nodeType": "BinaryOperation",
                        "operator": "==",
                        "rightExpression": {
                          "argumentTypes": null,
                          "hexValue": "74727565",
                          "id": 15,
                          "isConstant": false,
                          "isLValue": false,
                          "isPure": true,
                          "kind": "bool",
                          "lValueRequested": false,
                          "nodeType": "Literal",
                          "src": "226:4:0",
                          "subdenomination": null,
                          "typeDescriptions": {
                            "typeIdentifier": "t_bool",
                            "typeString": "bool"
                          },
                          "value": "true"
                        },
                        "src": "202:28:0",
                        "typeDescriptions": {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        }
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_bool",
                          "typeString": "bool"
                        }
                      ],
                      "id": 10,
                      "name": "require",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [
                        1218,
                        1219
                      ],
                      "referencedDeclaration": 1218,
                      "src": "194:7:0",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_require_pure$_t_bool_$returns$__$",
                        "typeString": "function (bool) pure"
                      }
                    },
                    "id": 17,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "194:37:0",
                    "typeDescriptions": {
                      "typeIdentifier": "t_tuple$__$",
                      "typeString": "tuple()"
                    }
                  },
                  "id": 18,
                  "nodeType": "ExpressionStatement",
                  "src": "194:37:0"
                },
                {
                  "id": 19,
                  "nodeType": "PlaceholderStatement",
                  "src": "237:1:0"
                }
              ]
            },
            "documentation": null,
            "id": 21,
            "name": "onlyApproved",
            "nodeType": "ModifierDefinition",
            "parameters": {
              "id": 9,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "187:0:0"
            },
            "src": "165:79:0",
            "visibility": "internal"
          },
          {
            "constant": false,
            "id": 23,
            "name": "toSet",
            "nodeType": "VariableDeclaration",
            "scope": 75,
            "src": "248:12:0",
            "stateVariable": true,
            "storageLocation": "default",
            "typeDescriptions": {
              "typeIdentifier": "t_string_storage",
              "typeString": "string"
            },
            "typeName": {
              "id": 22,
              "name": "string",
              "nodeType": "ElementaryTypeName",
              "src": "248:6:0",
              "typeDescriptions": {
                "typeIdentifier": "t_string_storage_ptr",
                "typeString": "string"
              }
            },
            "value": null,
            "visibility": "internal"
          },
          {
            "body": {
              "id": 33,
              "nodeType": "Block",
              "src": "286:40:0",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "id": 31,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "leftHandSide": {
                      "argumentTypes": null,
                      "baseExpression": {
                        "argumentTypes": null,
                        "id": 26,
                        "name": "approved",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 8,
                        "src": "293:8:0",
                        "typeDescriptions": {
                          "typeIdentifier": "t_mapping$_t_address_$_t_bool_$",
                          "typeString": "mapping(address => bool)"
                        }
                      },
                      "id": 29,
                      "indexExpression": {
                        "argumentTypes": null,
                        "expression": {
                          "argumentTypes": null,
                          "id": 27,
                          "name": "msg",
                          "nodeType": "Identifier",
                          "overloadedDeclarations": [],
                          "referencedDeclaration": 1215,
                          "src": "302:3:0",
                          "typeDescriptions": {
                            "typeIdentifier": "t_magic_message",
                            "typeString": "msg"
                          }
                        },
                        "id": 28,
                        "isConstant": false,
                        "isLValue": false,
                        "isPure": false,
                        "lValueRequested": false,
                        "memberName": "sender",
                        "nodeType": "MemberAccess",
                        "referencedDeclaration": null,
                        "src": "302:10:0",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address_payable",
                          "typeString": "address payable"
                        }
                      },
                      "isConstant": false,
                      "isLValue": true,
                      "isPure": false,
                      "lValueRequested": true,
                      "nodeType": "IndexAccess",
                      "src": "293:20:0",
                      "typeDescriptions": {
                        "typeIdentifier": "t_bool",
                        "typeString": "bool"
                      }
                    },
                    "nodeType": "Assignment",
                    "operator": "=",
                    "rightHandSide": {
                      "argumentTypes": null,
                      "hexValue": "74727565",
                      "id": 30,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": true,
                      "kind": "bool",
                      "lValueRequested": false,
                      "nodeType": "Literal",
                      "src": "316:4:0",
                      "subdenomination": null,
                      "typeDescriptions": {
                        "typeIdentifier": "t_bool",
                        "typeString": "bool"
                      },
                      "value": "true"
                    },
                    "src": "293:27:0",
                    "typeDescriptions": {
                      "typeIdentifier": "t_bool",
                      "typeString": "bool"
                    }
                  },
                  "id": 32,
                  "nodeType": "ExpressionStatement",
                  "src": "293:27:0"
                }
              ]
            },
            "documentation": null,
            "id": 34,
            "implemented": true,
            "kind": "constructor",
            "modifiers": [],
            "name": "",
            "nodeType": "FunctionDefinition",
            "parameters": {
              "id": 24,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "276:2:0"
            },
            "returnParameters": {
              "id": 25,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "286:0:0"
            },
            "scope": 75,
            "src": "265:61:0",
            "stateMutability": "nonpayable",
            "superFunction": null,
            "visibility": "public"
          },
          {
            "body": {
              "id": 43,
              "nodeType": "Block",
              "src": "377:24:0",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "id": 41,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "leftHandSide": {
                      "argumentTypes": null,
                      "id": 39,
                      "name": "toSet",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 23,
                      "src": "384:5:0",
                      "typeDescriptions": {
                        "typeIdentifier": "t_string_storage",
                        "typeString": "string storage ref"
                      }
                    },
                    "nodeType": "Assignment",
                    "operator": "=",
                    "rightHandSide": {
                      "argumentTypes": null,
                      "id": 40,
                      "name": "txt",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 36,
                      "src": "392:3:0",
                      "typeDescriptions": {
                        "typeIdentifier": "t_string_memory_ptr",
                        "typeString": "string memory"
                      }
                    },
                    "src": "384:11:0",
                    "typeDescriptions": {
                      "typeIdentifier": "t_string_storage",
                      "typeString": "string storage ref"
                    }
                  },
                  "id": 42,
                  "nodeType": "ExpressionStatement",
                  "src": "384:11:0"
                }
              ]
            },
            "documentation": null,
            "id": 44,
            "implemented": true,
            "kind": "function",
            "modifiers": [],
            "name": "setSomeText",
            "nodeType": "FunctionDefinition",
            "parameters": {
              "id": 37,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 36,
                  "name": "txt",
                  "nodeType": "VariableDeclaration",
                  "scope": 44,
                  "src": "351:17:0",
                  "stateVariable": false,
                  "storageLocation": "memory",
                  "typeDescriptions": {
                    "typeIdentifier": "t_string_memory_ptr",
                    "typeString": "string"
                  },
                  "typeName": {
                    "id": 35,
                    "name": "string",
                    "nodeType": "ElementaryTypeName",
                    "src": "351:6:0",
                    "typeDescriptions": {
                      "typeIdentifier": "t_string_storage_ptr",
                      "typeString": "string"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "350:19:0"
            },
            "returnParameters": {
              "id": 38,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "377:0:0"
            },
            "scope": 75,
            "src": "330:71:0",
            "stateMutability": "nonpayable",
            "superFunction": null,
            "visibility": "public"
          },
          {
            "body": {
              "id": 61,
              "nodeType": "Block",
              "src": "472:75:0",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "id": 55,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "lValueRequested": false,
                    "leftHandSide": {
                      "argumentTypes": null,
                      "baseExpression": {
                        "argumentTypes": null,
                        "id": 51,
                        "name": "approved",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 8,
                        "src": "479:8:0",
                        "typeDescriptions": {
                          "typeIdentifier": "t_mapping$_t_address_$_t_bool_$",
                          "typeString": "mapping(address => bool)"
                        }
                      },
                      "id": 53,
                      "indexExpression": {
                        "argumentTypes": null,
                        "id": 52,
                        "name": "_approved",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 46,
                        "src": "488:9:0",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      },
                      "isConstant": false,
                      "isLValue": true,
                      "isPure": false,
                      "lValueRequested": true,
                      "nodeType": "IndexAccess",
                      "src": "479:19:0",
                      "typeDescriptions": {
                        "typeIdentifier": "t_bool",
                        "typeString": "bool"
                      }
                    },
                    "nodeType": "Assignment",
                    "operator": "=",
                    "rightHandSide": {
                      "argumentTypes": null,
                      "hexValue": "74727565",
                      "id": 54,
                      "isConstant": false,
                      "isLValue": false,
                      "isPure": true,
                      "kind": "bool",
                      "lValueRequested": false,
                      "nodeType": "Literal",
                      "src": "501:4:0",
                      "subdenomination": null,
                      "typeDescriptions": {
                        "typeIdentifier": "t_bool",
                        "typeString": "bool"
                      },
                      "value": "true"
                    },
                    "src": "479:26:0",
                    "typeDescriptions": {
                      "typeIdentifier": "t_bool",
                      "typeString": "bool"
                    }
                  },
                  "id": 56,
                  "nodeType": "ExpressionStatement",
                  "src": "479:26:0"
                },
                {
                  "eventCall": {
                    "argumentTypes": null,
                    "arguments": [
                      {
                        "argumentTypes": null,
                        "id": 58,
                        "name": "_approved",
                        "nodeType": "Identifier",
                        "overloadedDeclarations": [],
                        "referencedDeclaration": 46,
                        "src": "531:9:0",
                        "typeDescriptions": {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      }
                    ],
                    "expression": {
                      "argumentTypes": [
                        {
                          "typeIdentifier": "t_address",
                          "typeString": "address"
                        }
                      ],
                      "id": 57,
                      "name": "AddedApproved",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 1032,
                      "src": "517:13:0",
                      "typeDescriptions": {
                        "typeIdentifier": "t_function_event_nonpayable$_t_address_$returns$__$",
                        "typeString": "function (address)"
                      }
                    },
                    "id": 59,
                    "isConstant": false,
                    "isLValue": false,
                    "isPure": false,
                    "kind": "functionCall",
                    "lValueRequested": false,
                    "names": [],
                    "nodeType": "FunctionCall",
                    "src": "517:24:0",
                    "typeDescriptions": {
                      "typeIdentifier": "t_tuple$__$",
                      "typeString": "tuple()"
                    }
                  },
                  "id": 60,
                  "nodeType": "EmitStatement",
                  "src": "512:29:0"
                }
              ]
            },
            "documentation": null,
            "id": 62,
            "implemented": true,
            "kind": "function",
            "modifiers": [
              {
                "arguments": null,
                "id": 49,
                "modifierName": {
                  "argumentTypes": null,
                  "id": 48,
                  "name": "onlyApproved",
                  "nodeType": "Identifier",
                  "overloadedDeclarations": [],
                  "referencedDeclaration": 21,
                  "src": "459:12:0",
                  "typeDescriptions": {
                    "typeIdentifier": "t_modifier$__$",
                    "typeString": "modifier ()"
                  }
                },
                "nodeType": "ModifierInvocation",
                "src": "459:12:0"
              }
            ],
            "name": "addApprovedAddress",
            "nodeType": "FunctionDefinition",
            "parameters": {
              "id": 47,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 46,
                  "name": "_approved",
                  "nodeType": "VariableDeclaration",
                  "scope": 62,
                  "src": "433:17:0",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_address",
                    "typeString": "address"
                  },
                  "typeName": {
                    "id": 45,
                    "name": "address",
                    "nodeType": "ElementaryTypeName",
                    "src": "433:7:0",
                    "stateMutability": "nonpayable",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "432:19:0"
            },
            "returnParameters": {
              "id": 50,
              "nodeType": "ParameterList",
              "parameters": [],
              "src": "472:0:0"
            },
            "scope": 75,
            "src": "405:142:0",
            "stateMutability": "nonpayable",
            "superFunction": 1037,
            "visibility": "public"
          },
          {
            "body": {
              "id": 73,
              "nodeType": "Block",
              "src": "633:35:0",
              "statements": [
                {
                  "expression": {
                    "argumentTypes": null,
                    "baseExpression": {
                      "argumentTypes": null,
                      "id": 69,
                      "name": "approved",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 8,
                      "src": "647:8:0",
                      "typeDescriptions": {
                        "typeIdentifier": "t_mapping$_t_address_$_t_bool_$",
                        "typeString": "mapping(address => bool)"
                      }
                    },
                    "id": 71,
                    "indexExpression": {
                      "argumentTypes": null,
                      "id": 70,
                      "name": "_addr",
                      "nodeType": "Identifier",
                      "overloadedDeclarations": [],
                      "referencedDeclaration": 64,
                      "src": "656:5:0",
                      "typeDescriptions": {
                        "typeIdentifier": "t_address",
                        "typeString": "address"
                      }
                    },
                    "isConstant": false,
                    "isLValue": true,
                    "isPure": false,
                    "lValueRequested": false,
                    "nodeType": "IndexAccess",
                    "src": "647:15:0",
                    "typeDescriptions": {
                      "typeIdentifier": "t_bool",
                      "typeString": "bool"
                    }
                  },
                  "functionReturnParameters": 68,
                  "id": 72,
                  "nodeType": "Return",
                  "src": "640:22:0"
                }
              ]
            },
            "documentation": null,
            "id": 74,
            "implemented": true,
            "kind": "function",
            "modifiers": [],
            "name": "isAddressApproved",
            "nodeType": "FunctionDefinition",
            "parameters": {
              "id": 65,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 64,
                  "name": "_addr",
                  "nodeType": "VariableDeclaration",
                  "scope": 74,
                  "src": "591:13:0",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_address",
                    "typeString": "address"
                  },
                  "typeName": {
                    "id": 63,
                    "name": "address",
                    "nodeType": "ElementaryTypeName",
                    "src": "591:7:0",
                    "stateMutability": "nonpayable",
                    "typeDescriptions": {
                      "typeIdentifier": "t_address",
                      "typeString": "address"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "590:15:0"
            },
            "returnParameters": {
              "id": 68,
              "nodeType": "ParameterList",
              "parameters": [
                {
                  "constant": false,
                  "id": 67,
                  "name": "",
                  "nodeType": "VariableDeclaration",
                  "scope": 74,
                  "src": "627:4:0",
                  "stateVariable": false,
                  "storageLocation": "default",
                  "typeDescriptions": {
                    "typeIdentifier": "t_bool",
                    "typeString": "bool"
                  },
                  "typeName": {
                    "id": 66,
                    "name": "bool",
                    "nodeType": "ElementaryTypeName",
                    "src": "627:4:0",
                    "typeDescriptions": {
                      "typeIdentifier": "t_bool",
                      "typeString": "bool"
                    }
                  },
                  "value": null,
                  "visibility": "internal"
                }
              ],
              "src": "626:6:0"
            },
            "scope": 75,
            "src": "564:104:0",
            "stateMutability": "view",
            "superFunction": 1044,
            "visibility": "public"
          }
        ],
        "scope": 76,
        "src": "77:594:0"
      }
    ],
    "src": "0:672:0"
  },
  "compiler": {
    "name": "solc",
    "version": "0.5.1+commit.c8a2cb62.Emscripten.clang"
  },
  "networks": {
    "1549552978304": {
      "events": {
        "0x4d8e6ba25400c3feba1ad107e99ee8bf523b9b3ed7f08d4fe26f44993363bc5d": {
          "anonymous": false,
          "inputs": [
            {
              "indexed": true,
              "name": "_approved",
              "type": "address"
            }
          ],
          "name": "AddedApproved",
          "type": "event",
          "signature": "0x4d8e6ba25400c3feba1ad107e99ee8bf523b9b3ed7f08d4fe26f44993363bc5d"
        }
      },
      "links": {},
      "address": "0x4C784852b134Eb41230cF5404a2285C1aBCB7303",
      "transactionHash": "0x466cce2ce49e64694edd89348e14250be437471f3a73fc5fd39331c57f282bc6"
    }
  },
  "schemaVersion": "3.0.1",
  "updatedAt": "2019-02-07T15:23:12.044Z",
  "devdoc": {
    "methods": {}
  },
  "userdoc": {
    "methods": {}
  }
}

module.exports = {

  callers

}
