var utils = require('./chain-interaction/utils.js');
var chain = require('./chain-interaction/chainMethods.js');
var deploy = require('./chain-interaction/deployContracts.js');

async function setCallersInMessengerContract() {

  try {

    var messengerInstance = await utils.getContractInstance('messenger');

    await utils.unlockLocalNode();

    var contractAddresses = await utils.getLatestContractsAddresses();

    chain.changeCallers(messengerInstance, contractAddresses[0], function(err, res) {

      if (err != undefined) {console.log(err); return}

      else {

        chain.getCallersAddress(messengerInstance, function(err2, res2) {

          if (err2) console.log(err2)

          else console.log(res2)

        })

      }

    });

  } catch(err) {

    console.log(err)

  }

}

async function setupContracts() {

  deploy.deployCallers(function (err1, res1) {

    if (err1) {console.log(err1); return;}

    console.log("Callers was deployed at: " + res1.contractAddress)

    deploy.deployMessenger(function (err2, res2) {

      if (err2) {console.log(err2); return;}

      console.log("Messenger was deployed at: " + res2.contractAddress)

    });

  });

}

async function test() {

  var instance = await utils.getContractInstance("messenger");

  await utils.unlockLocalNode();

  instance.methods.getNetworkMessageAt(0)
    .call({from: "0x816d1bd2eee5d05c5c2a5075a84a24ca519428ef"},
    async function (err, res) {

    if (err != undefined) { console.log(err) }

    else {

      var trans = await utils.bytesToString(res)

      console.log(trans)

    }

  });

}

test()

//setupContracts()

//checkIfAddressIsApproved();

//setCallersInMessengerContract()
