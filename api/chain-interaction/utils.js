var Web3Quorum = require('web3quorum');
var Web3 = require('web3');
var Personal = require('web3-eth-personal');
var fs = require('fs'), readline = require('readline');
const path = require("path");

var messengerABI = require('../abi/Messenger.js');
var approvedAbi = require('../abi/ApprovedCallers.js');

var pass = require('./pass.js');

var web3Quorum = new Web3Quorum(new Web3Quorum.providers.HttpProvider("http://10.50.0.2:22000"));
var classicWeb3 = new Web3(new Web3.providers.HttpProvider("http://10.50.0.2:22000"));

var defaultGas = 7000000;
var defaultUnlockTime = 30;

async function bytesToString(bts) {

  return classicWeb3.utils.hexToAscii(bts)

}

async function unlockLocalNode() {

  try {

    // QuorumMaker for now creates nodes with empty passwords
    // See here: https://github.com/synechron-finlabs/quorum-maker/issues/25#issuecomment-398012758
    await classicWeb3.eth.personal.unlockAccount(web3Quorum.eth.coinbase,
                                                 pass.password,
                                                 defaultUnlockTime);

  } catch(err) {

    return new Error(err);

  }

}

async function getContractInstance(contractName) {

  var addr;

  try {

    addr = getLatestContractsAddresses();

    if (addr.length < 2)
      return new Error('Not all necessary contracts were deployed');

    if (!classicWeb3.utils.isAddress(addr[0]) ||
        !classicWeb3.utils.isAddress(addr[1]))
      return new Error('One or more of the stored addresses is corrupted');

  } catch(err) {

    return new Error(err);

  }

  if (contractName === "messenger") {

    try {

      var messengerInstance = await new classicWeb3.eth.Contract(

        messengerABI.messenger.abi,
        addr[1],

        {from: web3Quorum.eth.coinbase}

      );

      return messengerInstance;

    } catch(err) {

      return new Error(err);

    }

  } else if (contractName === "callers") {

    try {

      var callersInstance = await new classicWeb3.eth.Contract(

        approvedAbi.callers.abi,
        addr[0],

        {from: web3Quorum.eth.coinbase}

      );

      return callersInstance;

    } catch(err) {

      return new Error(err);

    }

  } else return new Error('contractName was not correctly specified');

}

function getLatestContractsAddresses() {

  try {

    var lines = [];

    fs.readFileSync(path.resolve('./contractsAddresses.txt'))
      .toString()
      .split('\n')
      .forEach(function (line) {

        lines.push(line);

      })

    return lines;

  } catch(err) {

    return new Error(err);

  }

}

module.exports = {

  defaultGas,
  bytesToString,
  unlockLocalNode,
  getContractInstance,
  getLatestContractsAddresses

}
