var quorumAccounts = ["0x816d1bd2eee5d05c5c2a5075a84a24ca519428ef",
                      "0x54173d8f5d34eb4547b5fdbd198f3fc473decc5c",
                      "0x4acdc33ba219861d351b149bdf4aa7b6e53daf48"];

var quorumNodes = ["5QVzohOWyp2SrHThDO71KnbXl9xAj8hkfSR2xJRJJxU=",
                   "qeeIUtY1doGGtz4GigqzQ7kzEiUMjs6HLgvqCydB0BQ=",
                   "BekksHbXOTugsyOhP1VPwo8M+q8MqaXgJYUPN2IV4mY="];

module.exports = {

  quorumAccounts,
  quorumNodes

}
