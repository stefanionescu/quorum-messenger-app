var Web3Quorum = require('web3quorum');
var Web3 = require('web3');
var Personal = require('web3-eth-personal');

var utils = require('./utils.js');
var pass = require('./pass.js');

var web3Quorum = new Web3Quorum(new Web3Quorum.providers.HttpProvider("http://10.50.0.2:22000"));
var classicWeb3 = new Web3(new Web3.providers.HttpProvider("http://10.50.0.2:22000"))

var messengerAbi = require('../abi/Messenger.js');
var approvedAbi = require('../abi/ApprovedCallers.js');

async function deployMessenger(callback) {

  await classicWeb3.eth.personal.unlockAccount(web3Quorum.eth.coinbase, pass.password, 30)

    web3Quorum.eth.sendTransaction({

      from: web3Quorum.eth.coinbase,
      data: messengerAbi.messenger.bytecode,
      gas: 8000000

    }, function (e, contract) {

      if (e == undefined && contract != null && contract != undefined) {

        callback(undefined, web3Quorum.eth.getTransactionReceipt(contract))

      } else {

        callback("Messenger was not mined in time and could not fetch the address right away",
              undefined);

        }

      }

    );

}

async function deployCallers(callback) {

  await classicWeb3.eth.personal.unlockAccount(web3Quorum.eth.coinbase, pass.password, 30)

  web3Quorum.eth.sendTransaction({

    from: web3Quorum.eth.coinbase,
    data: approvedAbi.callers.bytecode,
    gas: 8000000

  }, function (e, contract) {

    if (e == undefined && contract != null && contract != undefined) {

      callback(undefined, web3Quorum.eth.getTransactionReceipt(contract))

    } else {

      callback("Callers was not mined in time and could not fetch the address right away",
              undefined);

    }

  })

}

module.exports = {

  deployMessenger,
  deployCallers

}
