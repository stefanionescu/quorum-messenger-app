var Web3Quorum = require('web3quorum');
var Web3 = require('web3');

var web3Quorum = new Web3Quorum(new Web3Quorum.providers.HttpProvider("http://10.50.0.2:22000"));
var classicWeb3 = new Web3(new Web3.providers.HttpProvider("http://10.50.0.2:22000"));

var utils = require('./utils.js');
var pass = require('./pass.js');

async function addApprovedAddress(instance, _approved, callback) {

  instance.methods.addApprovedAddress(_approved)
    .send({from: web3Quorum.eth.coinbase}, function(error, result) {

      if (error != undefined) { callback(error, undefined); }

      else callback(undefined, result);

  });

}

async function isAddressApproved(instance, _address, callback) {

  instance.methods.isAddressApproved(_address).call(function (err, res) {

    if (err != undefined) { callback(err, undefined); }

    else callback(undefined, res);

  });

}

async function changeCallers(instance, _callers, callback) {

  instance.methods.changeCallers(_callers).send({from: web3Quorum.eth.coinbase},
    function (err, res) {

    if (err != undefined) { callback(err, undefined); }

    else callback(undefined, res);

  });

}

async function sendNetworkMessage(instance, concatMsg, callback) {

  instance.methods.sendNetworkMessage(concatMsg).send({from: web3Quorum.eth.coinbase},
    function (err, res) {

    if (err != undefined) { callback(err, undefined); }

    else callback(undefined, res);

  });

}

async function sendMessage(instance, sender, receiver,
    stringifiedSender, stringifiedReceiver, msg, callback) {

  instance.methods.sendMessage(sender, receiver, stringifiedSender, stringifiedReceiver, msg)
    .send({from: web3Quorum.eth.coinbase},
    function (err, res) {

    if (err != undefined) { callback(err, undefined); }

    else callback(undefined, res);

  });

}

async function getCallersAddress(instance, callback) {

  instance.methods.getCallersAddress().call(function (err, res) {

    if (err != undefined) { callback(err, undefined); }

    else callback(undefined, res);

  });

}

async function getNetworkMessages(instance, callback) {

  instance.methods.getNetworkMessages().call(function (err, res) {

    if (err != undefined) { callback(err, undefined); }

    else callback(undefined, res);

  });

}

async function getMessages(instance, sender, recipient, callback) {

  instance.methods.getMessages(sender, recipient).call(function (err, res) {

    if (err != undefined) { callback(err, undefined); }

    else callback(undefined, res);

  });

}

module.exports = {

  addApprovedAddress,
  isAddressApproved,

  changeCallers,
  sendNetworkMessage,
  sendMessage,
  getCallersAddress,
  getNetworkMessages,
  getMessages

}
