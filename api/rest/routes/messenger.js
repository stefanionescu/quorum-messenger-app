const express = require('express');
const router = express.Router();

const MessengerController = require('../controllers/messengerController.js');

//Handle incoming requests

router.post('/public', MessengerController.messenger_send_public_message);

router.post('/private', MessengerController.messenger_send_private_message);

router.get('/public', MessengerController.messenger_get_public_messages);


/*

router.get('/private/count/:sender/:recipient',
  MessengerController.messenger_get_private_count);

router.get('/private/details/:sender/:recipient',
  MessengerController.messenger_get_private_details); */

module.exports = router;
