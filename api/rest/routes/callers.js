const express = require('express');
const router = express.Router();

const CallersController = require('../controllers/callersController');

//Handle incoming requests

router.get('/:addr', CallersController.callers_is_address_registered);

router.post('/:addr', CallersController.callers_register_address);

module.exports = router;
