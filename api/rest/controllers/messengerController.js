var chain = require('../../chain-interaction/chainMethods.js');
var utils = require('../../chain-interaction/utils.js');

exports.messenger_send_public_message = async (req, res, next) => {

  try {

    var instance = await utils.getContractInstance("messenger");

    await utils.unlockLocalNode();

    chain.sendNetworkMessage(instance,
      req.body.sender + "_" + req.body.receiver + "_" + req.body.msg,
      function(err, chainResult) {

        if (err != '' && err != undefined) {

          res.status(500).json({

            error: JSON.stringify(err)

          });

        }

        res.status(200).json({

          active: chainResult,
          request: {

            type: 'GET',
            description: "Get the details of a public message at a certain position",
            url: 'http://192.168.1.128:20000/messenger/public/:position'

          }

       });

     });

  } catch(err) {

    res.status(500).json({

      error: JSON.stringify(err)

    });

  }

}

exports.messenger_send_private_message = async (req, res, next) => {

  try {

    var instance = await utils.getContractInstance("messenger");

    await utils.unlockLocalNode();

    chain.sendMessage(instance, req.body.sender, req.body.receiver,
      req.body.sender.toString(), req.body.receiver.toString(), req.body.msg,
      function(err, chainResult) {

        if (err != '' && err != undefined) {

          res.status(500).json({

            error: JSON.stringify(err)

          });

        }

        res.status(200).json({

          active: chainResult,
          request: {

            type: 'GET',
            description: "Get the details of a private message at a certain position",
            url: 'http://192.168.1.128:20000/messenger/private/details/:sender/:recipient'

          }

       });

     });

  } catch(err) {

    res.status(500).json({

      error: JSON.stringify(err)

    });

  }

}

exports.messenger_get_public_messages = async (req, res, next) => {

  try {

    var instance = await utils.getContractInstance("messenger");

    await utils.unlockLocalNode();

    chain.getNetworkMessageAt(instance, function(err, chainResult) {

        if (err != '' && err != undefined) {

          res.status(500).json({

            error: JSON.stringify(err)

          });

        }

        res.status(200).json({

          active: chainResult,
          request: {

            type: 'POST',
            description: "Post a new public message",
            url: 'http://192.168.1.128:20000/messenger/public/:sender/:recipient/:msg'

          }

       });

     });

  } catch(err) {

    res.status(500).json({

      error: JSON.stringify(err)

    });

  }

}
