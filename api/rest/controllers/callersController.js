var chain = require('../../chain-interaction/chainMethods.js');
var utils = require('../../chain-interaction/utils.js');

exports.callers_is_address_registered = async (req, res, next) => {

  try {

    var instance = await utils.getContractInstance("callers");

    await utils.unlockLocalNode();

    chain.isAddressApproved(instance, req.params.addr, function(err, chainResult) {

        if (err != '' && err != undefined) {

          res.status(500).json({

            error: JSON.stringify(err)

          });

        }

        res.status(200).json({

          active: chainResult,
          request: {

            type: 'POST',
            url: 'http://192.168.1.128:20000/callers'

          }

       });

     });

  } catch(err) {

    res.status(500).json({

      error: JSON.stringify(err)

    });

  }

}

exports.callers_register_address = async (req, res, next) => {

  try {

    var instance = await utils.getContractInstance("callers");

    await utils.unlockLocalNode();

    chain.addApprovedAddress(instance, req.params.addr, function(err, chainResult) {

      if (err != '' && err != undefined) {

        res.status(500).json({

            error: JSON.stringify(err)

        });

      }

      res.status(200).json({

        tx: chainResult,
        request: {

          type: 'GET',
          url: 'http://192.168.1.128:20000/callers'

        }

     });

    });

  } catch(err) {

    res.status(500).json({

      error: JSON.stringify(err)

    });

  }

}
