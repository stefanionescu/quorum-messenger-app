const http = require('http');
const app = require('./app');

app.listen(20000, '192.168.1.128');

const port = process.env.PORT || 5000;

const server = http.createServer(app);

server.listen(port);
