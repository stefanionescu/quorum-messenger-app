const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const helmet = require('helmet');

const callersRoutes = require('./routes/callers');
const messengerRoutes = require('./routes/messenger');

const app = express();

app.use(morgan('dev'));
app.use(helmet())

app.use(bodyParser.urlencoded({extended: false}))
app.use(bodyParser.json());

//Routes that handle requests
app.use('/callers', callersRoutes);
app.use('/messenger', messengerRoutes);

app.use((req, res, next) => {

  const error = new Error('Not found');
  error.status = 404;

  next(error);

});

app.use((error, req, res, next) => {

  res.status(error.status || 500)

  res.json({

    error: {

      message: error.message

    }

  });

});

module.exports = app;
