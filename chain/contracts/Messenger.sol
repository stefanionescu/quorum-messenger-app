pragma solidity 0.5.1;

import "contracts/interfaces/IApprovedCallers.sol";
import "contracts/interfaces/IMessenger.sol";

import "contracts/zeppelin/Ownable.sol";
import "contracts/StringManipulator.sol";
import "contracts/Transformer.sol";

contract Messenger is IMessenger, Ownable, StringManipulator, Transformer {

  mapping(address => mapping(address => bytes[])) messages;

  bytes[] networkMessages;

  IApprovedCallers callers;

  modifier onlyApprovedCaller {

    require(callers.isAddressApproved(msg.sender) == true);
    _;

  }

  constructor() public {}

  function changeCallers(address _callers) public onlyOwner {

    require(this.isContract(_callers) == true, "The address is not a contract");

    callers = IApprovedCallers(_callers);

    emit ChangedCallersContract(_callers);

  }

  function sendNetworkMessage(string memory sender,
                              string memory receiver,
                              string memory msg) public onlyApprovedCaller {

    bytes memory concatAgents = concatBytes(bytes(sender), bytes(receiver));

    bytes memory finalConcat = concatBytes(concatAgents, bytes(msg));

    networkMessages.push(finalConcat);

  }

  function sendMessage(address sender,
                       address recipient,
                       string memory stringifiedSender,
                       string memory stringifiedReceiver,
                       string memory _encryptedMessage)
                       public onlyApprovedCaller {

    bytes memory concatAgents = concatBytes(bytes(stringifiedSender), bytes(stringifiedReceiver));

    bytes memory finalConcat = concatBytes(concatAgents, bytes(_encryptedMessage));

    messages[sender][recipient].push(finalConcat);

  }

  //PRIVATE

  function isContract(address _addr) public returns (bool) {

    uint32 size;

    assembly {

      size := extcodesize(_addr)

    }

    return (size > 0);

  }

  //GETTERS

  function getCallersAddress() public view returns (address) {

    return address(callers);

  }

  function getNetworkMessageAt(uint256 position) public view returns (bytes memory) {

    if (position >= networkMessages.length) return bytes("");

    return networkMessages[position];

  }

  function getMessageAt(address sender, address recipient, uint256 position)
    public view returns (bytes memory) {

    if (position >= messages[sender][recipient].length) return bytes("");

    return messages[sender][recipient][position];

  }

}
