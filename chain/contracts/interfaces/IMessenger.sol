pragma solidity 0.5.1;
pragma experimental ABIEncoderV2;

contract IMessenger {

  event ChangedCallersContract(address indexed _callers);

  event SentMessage(address sender, address recipient, string _encryptedMessage);

  event NetworkMessage(address sender, address recipient);


  function changeCallers(address _callers) public;

  function sendNetworkMessage(string memory sender,
                              string memory receiver,
                              string memory msg) public;

  function sendMessage(address sender,
                       address recipient,
                       string memory stringifiedSender,
                       string memory stringifiedReceiver,
                       string memory _encryptedMessage)
                       public;

  function getCallersAddress() public view returns (address);

  function getNetworkMessageAt(uint256 position) public view returns (bytes memory);

  function getMessageAt(address sender, address recipient, uint256 position)
    public view returns (bytes memory);

}
