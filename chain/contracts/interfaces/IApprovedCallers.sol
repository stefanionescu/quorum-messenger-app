pragma solidity 0.5.1;

contract IApprovedCallers {

  event AddedApproved(address indexed _approved);

  function addApprovedAddress(address _approved) public;

  function isAddressApproved(address _addr) public view returns (bool);

}
