pragma solidity 0.5.1;

import "contracts/interfaces/IApprovedCallers.sol";

contract ApprovedCallers is IApprovedCallers {

  mapping(address => bool) approved;

  modifier onlyApproved {

    require(approved[msg.sender] == true);
    _;

  }

  string toSet;

  constructor() public {

    approved[msg.sender] = true;

  }

  function setSomeText(string memory txt) public {

    toSet = txt;

  }

  function addApprovedAddress(address _approved) public onlyApproved {

    approved[_approved] = true;

    emit AddedApproved(_approved);

  }

  //GETTERS

  function isAddressApproved(address _addr) public view returns (bool) {

    return approved[_addr];

  }

}
