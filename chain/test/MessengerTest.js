var Messenger = artifacts.require("contracts/Messenger.sol");
var ApprovedCallers = artifacts.require("contracts/ApprovedCallers.sol");

const EthCrypto = require('eth-crypto');

var Web3 = require("web3");

const web3 = new Web3("ws://localhost:8545");

var devPrivateKeys = [

  "0x80113ebd0983a0aebbe9d7e16276bc7f8def180723d78f15c6b392dd191e7466",
  "0xa34c5157521ac1e4eae5be83f95d78da64765a0cabba34f7b917505352de8ee7",
  "0xca6c909c0bead1906a103b29f545c9c6b1cd157ce524d5aae2de1803a99e874d",
  "0x9a83993bd1c34e6a920acd533a493c49fd84f74f4d6ac2a24d6f26eba27659e9",
  "0xcacb122e1ef6107839dc4ed89508bf90b77c43bccea72bec47141e76efd8e420",
  "0xfb41eb6fb17ce2b1d504463a7cc96e7d3488fa1793e7ac9bc853028a59297005"

];

let callers, messenger;

contract('Messenger', function(accounts) {

    beforeEach(async () => {

      callers = await ApprovedCallers.new({from: accounts[0]});

      messenger = await Messenger.new({from: accounts[0]});


      await callers.addApprovedAddress(accounts[1], {from: accounts[0]});

      await messenger.changeCallers(callers.address, {from: accounts[0]});

    })

    it("should send an encrypted message between different addresses", async () => {

      let randLength;
      let sendMessageResult;

      for (var i = 0; i < 4; i++) {

        randLength = Math.floor(Math.random() * (2000 - 1000) + 1000);

        sendMessageResult = await sendMessageAndCheckIt(makeRandomMessage(randLength), accounts, i);

        assert(sendMessageResult == true);

      }

    })

    it("should send a network message successfuly", async () => {

      var msg = "Network Message";

      var composedStr = accounts[0].toString() + "_" + accounts[1].toString() + "_" + msg

      await messenger.sendNetworkMessage(accounts[0].toString() + "_",
                                         accounts[1].toString() + "_",
                                         msg);

      var messageData = await messenger.getNetworkMessageAt(0);

      assert(web3.utils.hexToAscii(messageData) === composedStr)

    })

})

async function sendMessageAndCheckIt(msg, accounts, pos) {

  var acc1PublicKey = EthCrypto.publicKeyByPrivateKey(
    devPrivateKeys[1]
  );

  var encryptedMsg = await EthCrypto.encryptWithPublicKey(
    acc1PublicKey, // publicKey
    msg// message
  );

  //Transforms the encrypted message object into a string which is easy to store in a contract
  var stringifiedMsg = EthCrypto.cipher.stringify(encryptedMsg);

  await messenger.sendMessage(accounts[0],
                              accounts[1],
                              accounts[0].toString() + "_",
                              accounts[1].toString() + "_",
                              stringifiedMsg,
                              {from: accounts[0]});

  var privateMessage = web3.utils.hexToAscii(
      await messenger.getMessageAt(accounts[0], accounts[1], pos) );

  var splitMsg = privateMessage.split("_")

  var parsedSecret = EthCrypto.cipher.parse(splitMsg[2]);

  var recoveredMsg = await EthCrypto.decryptWithPrivateKey(
        devPrivateKeys[1],
        parsedSecret
    );

  return (

    splitMsg[0] === accounts[0] &&
    splitMsg[1] === accounts[1] &&
    splitMsg[2] === stringifiedMsg &&
    recoveredMsg === msg

  );

}

function makeRandomMessage(len) {

  var text = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  for (var i = 0; i < len; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));

  return text;

}
