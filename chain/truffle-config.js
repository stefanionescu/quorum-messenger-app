// const HDWalletProvider = require('truffle-hdwallet-provider');

module.exports = {

  networks: {

    development: {
      host: "localhost",
      port: 8545,
      network_id: "*",
      from: "0x6259ac218eed8caf47e26246d7e13c1df70165f2",
      gas: 8000000
    },

    node1Public: {
      host: "10.50.0.2",
      port: 22000,
      network_id: "*",
      gasPrice: 0,
      gas: 8000000
    },

    node1Private: {
      host: "10.50.0.3",
      port: 22000,
      network_id: "*",
      gasPrice: 0,
      gas: 8000000
    }

  },

  compilers: {
    solc: {
      version: "0.5.1",
    },
  }

}
