var Messenger = artifacts.require("contracts/Messenger.sol");
var ApprovedCallers = artifacts.require("contracts/ApprovedCallers.sol");

var quorumAccounts = ["0xd60fb1f9b4f0aa0d26fd4011a44f6270d9f3d377",
                      "0x0a8405f77e5ac1ee0576a8e6dd4b62b6df566aab",
                      "0x9ebd8ac3ec752d1f9dc7d43016ce252c8f55b3b2"];

module.exports = async function(deployer, network, accounts) {

  if (network == "development") {

    deployer.deploy(ApprovedCallers).then(function() {
      return deployer.deploy(Messenger).then(async function() {

        var approved = await ApprovedCallers.deployed();

        await approved.addApprovedAddress(accounts[1], {from: accounts[0]});

        await approved.addApprovedAddress(accounts[2], {from: accounts[0]});

        var messenger = await Messenger.deployed();

        await messenger.changeCallers(approved.address, {from: accounts[0]})

    }) })

  }

}

async function unlockNode(callback) {

  var Web3Quorum = require('web3quorum');
  var Web3 = require('web3');
  var Personal = require('web3-eth-personal');

  var personalInstance = new Personal('http://10.50.0.2:22000');
  var web3Quorum = new Web3Quorum(new Web3Quorum.providers.HttpProvider("http://10.50.0.2:22000"));
  var classicWeb3 = new Web3(new Web3.providers.HttpProvider("http://10.50.0.2:22000"))

  //Unlocking node 1

  try {

    await classicWeb3.eth.personal.unlockAccount(quorumAccounts[0], "", 60)
    .then(callback(null, true));

  } catch(err) {

    callback(err, false);

  }

}
